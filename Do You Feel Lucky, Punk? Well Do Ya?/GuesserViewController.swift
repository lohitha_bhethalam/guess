//
//  FirstViewController.swift
//  Do You Feel Lucky, Punk? Well Do Ya?
//
//  Created by Bhethalam,Lohitha on 2/27/19.
//  Copyright © 2019 Bhethalam,Lohitha. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {
    @IBOutlet weak var guesserTF: UITextField!
    @IBOutlet weak var guesserLBL: UILabel!
    @IBAction func amIRight(_sender: Any){
        if let message = Int(guesserTF.text!){
            let output = Guesser.shared.amIRight(guess: Int(message))
            if output == Result.correct{
                guesserLBL.text = output.rawValue
                displayMessage()
                Guesser.shared.createNewProblem()
            }
            else if message < 1 || message > 10{
                errorMessage()
            }
            else{
                guesserLBL.text = output.rawValue
            }}
            else {
                errorMessage()
            }
        }
    
    @IBAction func createNew(_sender: Any){
        guesserLBL.text = ""
        guesserTF.text = ""
        Guesser.shared.createNewProblem()
    }
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",
                                      message: "You got it in \(Guesser.shared.numAttempts) tries",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func errorMessage(){
        let alert = UIAlertController(title: "ERROR!",
                                      message: "Enter valid input (number between 1-10)",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

