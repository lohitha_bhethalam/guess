//
//  StatisticsViewController.swift
//  Do You Feel Lucky, Punk? Well Do Ya?
//
//  Created by Bhethalam,Lohitha on 2/27/19.
//  Copyright © 2019 Bhethalam,Lohitha. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {

    @IBOutlet weak var minLBL: UITextField!
    
    @IBOutlet weak var maxLBL: UITextField!
    
    
    @IBOutlet weak var meanLBL: UITextField!
    
    @IBOutlet weak var stdDevLBL: UITextField!
    
    
    
    @IBAction func clearStatistics(_ sender: Any) {
        minLBL.text = ""
        maxLBL.text = ""
        meanLBL.text = ""
        stdDevLBL.text = ""
        
    }
    override func viewWillAppear(_ Animated: Bool){
       minLBL.text = String(Guesser.shared.minimumNumAttempts())
        maxLBL.text = String(Guesser.shared.maximumNumAttempts())
        var total = 0
        for numbers in 0 ..< Guesser.shared.numGuesses(){
            total += Guesser.shared.guess(index: numbers ).numAttemptsRequired
        }
       let mean  = Double(total)/Double(Guesser.shared.numGuesses())
        meanLBL.text = String(mean)
        var standardDev = 0.0
        for numbers in 0 ..< Guesser.shared.numGuesses(){
            standardDev += pow(Double(Guesser.shared.guess(index: numbers).numAttemptsRequired) - mean, 2)
            
        }
        stdDevLBL.text = String(standardDev/Double(Guesser.shared.numGuesses()))
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
